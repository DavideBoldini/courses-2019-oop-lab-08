package it.unibo.oop.lab.mvcio;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * 
 */
public class Controller {

    /*
     * This class must implement a simple controller responsible of I/O access. It
     * considers a single file at a time, and it is able to serialize objects in it.
     * 
     * Implement this class with:
     * 
     * 1) A method for setting a File as current file
     * 
     * 2) A method for getting the current File
     * 
     * 3) A method for getting the path (in form of String) of the current File
     * 
     * 4) A method that gets a String as input and saves its content on the current
     * file. This method may throw an IOException.
     * 
     * 5) By default, the current file is "output.txt" inside the user home folder.
     * A String representing the local user home folder can be accessed using
     * System.getProperty("user.home"). The separator symbol (/ on *nix, \ on
     * Windows) can be obtained as String through the method
     * System.getProperty("file.separator"). The combined use of those methods leads
     * to a software that runs correctly on every platform.
     */

    private File currentFile;
    private String currentPath;
    private static final String PATH_DEFAULT = System.getProperty("user.dir") + System.getProperty("file.separator")
            + "output.txt";

    public Controller() {
        this.currentFile = new File(PATH_DEFAULT);
        this.currentPath = PATH_DEFAULT;
    }

    public final void setFile(final File f) {
        File newFile = new File(f.getAbsolutePath());
        try {
            newFile.createNewFile();
        } catch (Exception e) {
            System.out.println(e);
        }
        this.currentFile = newFile;
        this.currentPath = newFile.getAbsolutePath();
    }

    public final File getFile() {
        return this.currentFile;
    }

    public final String getPathFile() {
        return this.currentFile.getAbsolutePath();
    }

    public final void writeFile(final String inputString) throws IOException {
        try (FileWriter writer = new FileWriter(currentPath); BufferedWriter bw = new BufferedWriter(writer)) {
            bw.write(inputString);
        }
    }

}
