package it.unibo.oop.lab.advanced;

import java.util.List;
import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;



/**
 */
public final class DrawNumberApp implements DrawNumberViewObserver {

    //private static final int MIN;
    //private static final int MAX;
    //private static final int ATTEMPTS;
    private final DrawNumber model;
    private final DrawNumberView view;
    private List<Integer> values;

    /**
     * 
     */

    public DrawNumberApp() {
        this.setValues();
        this.model = new DrawNumberImpl(values.get(0), values.get(1), values.get(2));
        this.view = new DrawNumberViewImpl();
        this.view.setObserver(this);
        this.view.start();
    }

    @Override
    public void newAttempt(final int n) {
        try {
            final DrawResult result = model.attempt(n);
            this.view.result(result);
        } catch (IllegalArgumentException e) {
            this.view.numberIncorrect();
        } catch (AttemptsLimitReachedException e) {
            view.limitsReached();
        }
    }

    @Override
    public void resetGame() {
        this.model.reset();
    }

    @Override
    public void quit() {
        System.exit(0);
    }

    private void setValues() {
        values = new ArrayList<Integer>();
        try {
            final Scanner scan = new Scanner(new File("res/config.yml"));
            while (scan.hasNext()) {
                String s = scan.next();
                if (s.matches("[0-9]+")) {
                    values.add(Integer.parseInt(s));
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * @param args
     *            ignored
     * @throws FileNotFoundException 
     */
    public static void main(final String... args){
        new DrawNumberApp();

    }

}
