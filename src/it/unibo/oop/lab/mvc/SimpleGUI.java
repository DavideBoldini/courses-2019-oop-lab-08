package it.unibo.oop.lab.mvc;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * A very simple program using a graphical interface.
 * 
 */
public final class SimpleGUI {

    private final JFrame frame = new JFrame(TITLE);
    private static final String TITLE = "A very simple GUI application";
    private static final int PROPORTION = 5;
    private final ControllerImpl controller;

    /*
     * Once the Controller is done, implement this class in such a way that:
     * 
     * 1) I has a main method that starts the graphical application
     * 
     * 2) In its constructor, sets up the whole view
     * 
     * 3) The graphical interface consists of a JTextField in the upper part of the
     * frame, a JTextArea in the center and two buttons below it: "Print", and
     * "Show history". SUGGESTION: Use a JPanel with BorderLayout
     * 
     * 4) By default, if the graphical interface is closed the program must exit
     * (call setDefaultCloseOperation)
     * 
     * 5) The behavior of the program is that, if "Print" is pressed, the controller
     * is asked to show the string contained in the text field on standard output.
     * If "show history" is pressed instead, the GUI must show all the prints that
     * have been done to this moment in the text area.
     * 
     */
    public SimpleGUI() {
        this.controller = new ControllerImpl();

        final JPanel pNorth = new JPanel();
        pNorth.setLayout(new BorderLayout());
        final JTextField textField = new JTextField();
        pNorth.add(textField, BorderLayout.CENTER);

        final JPanel pCenter = new JPanel();
        pCenter.setLayout(new BorderLayout());
        final JTextArea textArea = new JTextArea();
        textArea.setEditable(false);
        pCenter.add(textArea);

        final JPanel pSouth = new JPanel();
        pSouth.setLayout(new BorderLayout());
        final JButton print = new JButton("Print");
        pSouth.add(print, BorderLayout.CENTER);

        print.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
                try {
                    controller.setNextStringToPrint(textField.getText());
                    controller.currentString();
                    textField.setText(null);
                } catch (IOException e1) {
                    JOptionPane.showMessageDialog(frame, "Inserisci Stringa nella JText Field", "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        final JButton showHistory = new JButton("Show History");
        pSouth.add(showHistory, BorderLayout.EAST);

        showHistory.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
               textArea.setText(controller.getHistory().toString() + "\n"); 
            }
        });

        frame.add(pNorth, BorderLayout.NORTH);
        frame.add(pCenter, BorderLayout.CENTER);
        frame.add(pSouth, BorderLayout.SOUTH);

    }

    /**
     * builds a new {@link SimpleGUI}.
     */
    public void display() {

        /*
         * Make the frame half the resolution of the screen. This very method is enough
         * for a single screen setup. In case of multiple monitors, the primary is
         * selected.
         * 
         * In order to deal coherently with multimonitor setups, other facilities exist
         * (see the Java documentation about this issue). It is MUCH better than
         * manually specify the size of a window in pixel: it takes into account the
         * current resolution.
         */
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        frame.setSize(sw / PROPORTION, sh / PROPORTION);

        /*
         * Instead of appearing at (0,0), upper left corner of the screen, this flag
         * makes the OS window manager take care of the default positioning on screen.
         * Results may vary, but it is generally the best choice.
         */
        frame.setLocationByPlatform(true);
        frame.setVisible(true);
    }

    public static void main(final String... args) {
        new SimpleGUI().display();
    }
}
