package it.unibo.oop.lab.mvc;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class ControllerImpl implements Controller {

    private final List<String> historyList;
    private String currentString;

    public ControllerImpl() {
        this.historyList = new LinkedList<String>();
        this.currentString = null;
    }

    public final void setNextStringToPrint(final String nextString) throws IOException {
        if (nextString != null && !nextString.isEmpty()) {
            this.historyList.add(nextString);
            this.currentString = nextString;
        } else {
            throw new IOException();
        }
    }

    public final String getNextString() {
        return this.currentString;
    }

    public final List<String> getHistory() {
        return this.historyList;
    }

    public final void currentString() throws IllegalStateException {
        if (currentString != null && !currentString.isEmpty()) {
            System.out.println(currentString);
        } else {
            throw new IllegalStateException();
        }
    }
}
